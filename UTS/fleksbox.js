import React, { useState} from 'react';
import {View , StyleSheet, Image, ScrollView, TouchableOpacity, Text} from 'react-native';
import Suka from './suka';
import Button from './button';
const FlexBox = () =>{
    const [nurul, setNurul] = useState(0);
        return(
            <View style= {styles.container}>
                <View style={styles.flexOne}>
                    <Image source={require('/Users/user/nurul/src/assets/ats.jpg')} style={styles.photos}/>
                </View>
                <View style={styles.flexTwo}>
                    <ScrollView>
                        <ScrollView horizontal>
                        <Image source={require('/Users/user/nurul/src/assets/huu.jpg')} style={styles.status}/>
                        <Image source={require('/Users/user/nurul/src/assets/gmbr8.jpg')} style={styles.cerita}/>
                        <Image source={require('/Users/user/nurul/src/assets/gmbr6.jpg')} style={styles.cerita}/>
                        <Image source={require('/Users/user/nurul/src/assets/gmbr9.jpg')} style={styles.cerita}/>
                        <Image source={require('/Users/user/nurul/src/assets/gmbr7.jpg')} style={styles.cerita}/>
                        <Image source={require('/Users/user/nurul/src/assets/gmr6.jpg')} style={styles.cerita}/>
                        <Image source={require('/Users/user/nurul/src/assets/gmbr10.jpg')} style={styles.cerita}/>
                        </ScrollView>

                    <Image source={require('/Users/user/nurul/src/assets/gmr1.jpg')} style={styles.img}/>
                    <Button onButtonPress={() => setNurul(nurul+1)}/>
                    <Suka nurulb={nurul}/>

                    <Image source={require('/Users/user/nurul/src/assets/gmbr2.jpg')} style={styles.img}/>
                    <Button onButtonPress={() => setNurul(nurul+1)}/>
                    <Suka nurulb={nurul}/>

                    <Image source={require('/Users/user/nurul/src/assets/gmr2.jpg')} style={styles.img}/>
                    <Button onButtonPress={() => setNurul(nurul+1)}/>
                    <Suka nurulb={nurul}/>

                    <Image source={require('/Users/user/nurul/src/assets/gmr5.jpg')} style={styles.photo}/>
                    <Button onButtonPress={() => setNurul(nurul+1)}/>
                    <Suka nurulb={nurul}/>

                    <Image source={require('/Users/user/nurul/src/assets/gmr4.jpg')} style={styles.photo}/>
                    <Button onButtonPress={() => setNurul(nurul+1)}/>
                    <Suka nurulb={nurul}/>
                    
                </ScrollView>
                </View>
                <View style={styles.flexThree}>
                    <Image source={require('/Users/user/nurul/src/assets/bwh.jpg')} style={styles.gambar}/>
                </View>
            </View>
        );
   
};

const styles = StyleSheet.create({
    container: {
        flex : 1,
        backgroundColor:'purple',
    },
  flexOne:{
        flex: 1,
        backgroundColor:'white'
   },
   flexTwo :{
       flex : 8,
       backgroundColor:'white'
   },
   flexThree:{
       flex:1,
       backgroundColor:'white'
   },
   photos:{
       width : 364,
       height : 56,
   },
   img : {
        width : 360,
        height : 420,
        marginTop : 20
   },
   photo : {
       width : 400,
       height : 550,
       marginTop:10
   },
   gambar :{
       width : 359,
       height : 44
   },
   cerita : {
       width : 69,
       height : 69,
       borderRadius : 35,
       borderWidth : 3,
       borderColor : 'red',
       marginLeft : 10
   },
   status : {
       width : 85,
       height : 78,
       borderRadius : 37,
       borderWidth : 4,
       backgroundColor: 'white',
       marginLeft : 10
   },
   love : {
       width:20,
       height:20,
       marginLeft:10,
       marginTop:5
   }
   
});

export default FlexBox;
