import React from 'react';
import {ScrollView, StyleSheet, Image, TouchableOpacity} from 'react-native';

const button =(props) => {
    return(
        <ScrollView horizontal>
            <TouchableOpacity onPress={props.onButtonPress}>
            <Image source={require('../assets/like.jpg')} style={{width : 24, height : 20, marginLeft : 1,marginTop : 2}}/>
            </TouchableOpacity>
            <TouchableOpacity>
            <Image source={require('../assets/comn.jpg')} style={{width : 24, height : 23, marginLeft : 8,marginTop : 2}}/>
            </TouchableOpacity>
            <TouchableOpacity>
            <Image source={require('../assets/share.jpg')} style={{width : 28, height : 23, marginLeft : 8,marginTop : 2}}/> 
            </TouchableOpacity>
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    like : {
        width : 40,
        height : 40,
        marginLeft : 10,
        marginTop : 1
    }
});

export default button;