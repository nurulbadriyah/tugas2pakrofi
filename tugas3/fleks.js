import React, {Component} from 'react';
import {View , StyleSheet, Image, ScrollView} from 'react-native';

class FlexBox extends Component{
    render() {
        return(
            <View style= {styles.container}>
                <View style={styles.flexOne}>
                    <Image source={require('/Users/Lonovo/nubad/src/assets/ats.jpg')} style={styles.photos}/>
                </View>
                <View style={styles.flexTwo}>
                    <Image source={require('/Users/Lonovo/nubad/src/assets/sts.jpg')} style={styles.img}/>
                </View>
                <View style={styles.flexThree}>
                <ScrollView>
                    <Image source={require('/Users/Lonovo/nubad/src/assets/gmr1.jpg')} style={styles.photo}/>
                    <Image source={require('/Users/Lonovo/nubad/src/assets/gmbr2.jpg')} style={styles.photo}/>
                    <Image source={require('/Users/Lonovo/nubad/src/assets/gmr2.jpg')} style={styles.photo}/>
                    <Image source={require('/Users/Lonovo/nubad/src/assets/gmr5.jpg')} style={styles.photo}/>
                    <Image source={require('/Users/Lonovo/nubad/src/assets/gmr4.jpg')} style={styles.photo}/>
                </ScrollView>
                </View>
                <View style={styles.flexFour}>
                    <Image source={require('/Users/Lonovo/nubad/src/assets/bwh.jpg')} style={styles.gambar}/>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex : 1,
        backgroundColor:'purple'
    },
  flexOne:{
        flex: 1,
        backgroundColor:'white'
   },
   flexTwo :{
       flex : 2,
       backgroundColor:'white'
   },
   flexThree:{
       flex:8,
       backgroundColor:'white'
   },
   flexFour:{
       flex:1,
       backgroundColor:'white'
   },
   photos:{
       width : 364,
       height : 56,
   },
   img : {
        width : 358,
        height : 93
   },
   photo : {
       width : 368,
       height : 592
   },
   gambar :{
       width : 359,
       height : 44
   },
   
});

export default FlexBox;