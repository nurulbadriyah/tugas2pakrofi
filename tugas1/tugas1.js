import React from 'react';
import { Text, TextInput, View, Image } from 'react-native';

const Cat = () => {
  return (
    <View style={{ flex: 1, justifyContent: "center", alignItems: "center", backgroundColor: "red" }}>
      <Image
          source={{
            uri: 'https://reactnative.dev/docs/assets/p_cat2.png',
          }}
          style={{ width: 270, height: 270 }}
        />

      <Text>Hello, ini project pertama saya... tolong katakan sesuatuuuu!!!</Text>
      <TextInput
        style={{
          height: 40,
          width: 70,
          borderColor: 'black',
          backgroundColor: 'pink',
          borderWidth: 1
        
        }}
        defaultValue="Klik disini"
      />
    </View>
  );
}

export default Cat;
